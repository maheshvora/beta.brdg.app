import { createTheme } from "@material-ui/core/styles";
const font = "Mulish, sans-serif";
const theme = createTheme({
    palette: {
        type: "light",
        primary: {
            main: "#0038ff",
        },
    },
    typography: {
        fontFamily: font,
        h1: {
            fontSize: 48,
        },
        h2: {
            fontSize: 28,
        },
        h3: {
            fontSize: 24,
        },
        h4: {
            fontSize: 22,
        },
        h5: {
            fontSize: 20,
        },
        h6: {
            fontSize: 18,
        },
        subtitle1: {
            fontSize: 16,
        },
        subtitle2: {
            fontSize: 12,
        },
        button: {
            fontSize: 22,
        },
    },
    breakpoints: {
        values: {
            xs: 0,
            sm: 600,
            md: 960,
            lg: 1388,
            xl: 1920,
        },
    },
    shape: {
        borderRadius: 8,
    },
    shadows: ["none", "3px 3px 17px -9px rgba(0, 0, 0, 0.25)"],
    overrides: {
        MuiButton: {
            root: { textTransform: "none" },
            containedPrimary: {
                backgroundColor: "#0038ff",
                backgroundImage: "linear-gradient(247deg, #1f37ff, #4388ff)",
                boxShadow: "0 2px 10px 0 rgb(0 0 0 / 25%)",
                color: "hsla(0, 0%, 100%, 0.9)",
                fontSize: "16px",
                lineHeight: "24px",
                fontWeight: "600",
                textAlign: "center",
                letterSpacing: "0.4px",
            },

            outlinedPrimary: {
                fontSize: "16px",
                lineHeight: "24px",
                fontWeight: "600",
                textAlign: "center",
                letterSpacing: "0.4px",
            },
        },
    },
});
export default theme;
