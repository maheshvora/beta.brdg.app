import Home from "../pages/home";
import { ReactComponent as USFlagIcon } from "../assets/svgs/us-flag.svg";
import { ReactComponent as FranceFlagIcon } from "../assets/svgs/france-flag.svg";
export const links = [{ path: "/", element: <Home />, menu: "Bridge" }];
export const headerMenus = [
    {
        name: "Home",
    },
];
export const languageMenus = [
    {
        slug: "english",
        name: "English",
        icon: <USFlagIcon />,
    },
    {
        slug: "french",
        name: "French",
        icon: <FranceFlagIcon />,
    },
];

export const footerMenus = [
    {
        name: "Terms",
    },
    {
        name: "Privacy",
    },
    {
        name: "Cookies",
    },
];
