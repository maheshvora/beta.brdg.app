import React from "react";
import { ReactComponent as ColorLogo } from "../../../assets/svgs/bridge-logo-color.svg";
import { ReactComponent as PlaneLogo } from "../../../assets/svgs/bridge-logo-plane.svg";
import { siteUrl } from "../../../helpers/constants";
import { oneOf } from "prop-types";
const Logo = ({ mode }) => {
    return (
        <a href={siteUrl}>
            {mode === "color" && <ColorLogo />}
            {mode === "plane" && <PlaneLogo />}
        </a>
    );
};

/*
 * PropTypes validations
 * @property {oneOf} mode - Select the mode the logo Plane or Color
 */
Logo.propTypes = {
    mode: oneOf(["color", "plane"]),
};

Logo.defaultProps = {
    mode: "color",
};

export default Logo;
