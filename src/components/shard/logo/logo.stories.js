import React from "react";
import Logo from "./index.js";

const LogoStory = {
    title: "Elements/Logo",
    component: Logo,
    parameters: {
        actions: {
            handles: ["click"],
        },
    },
};

const Template = (args) => <Logo {...args} />;

export const ColorLogo = Template.bind({});
ColorLogo.args = {
    mode: "color",
};

export const PlaneLogo = Template.bind({});
PlaneLogo.args = {
    mode: "plane",
};

export default LogoStory;
