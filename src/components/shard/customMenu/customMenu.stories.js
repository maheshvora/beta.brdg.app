import React from "react";
import CustomMenu from "./index.js";

const CustomMenuStory = {
    title: "Elements/CustomMenu",
    component: CustomMenu,
};

const Template = (args) => <CustomMenu {...args} />;

export const HeaderMenu = Template.bind({});
HeaderMenu.args = {
    type: "header",
};

export const FooterMenu = Template.bind({});
FooterMenu.args = {
    type: "footer",
};

export const LanguageMenu = Template.bind({});
LanguageMenu.args = {
    type: "language",
};

export default CustomMenuStory;
