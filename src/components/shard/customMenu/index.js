import React from "react";
import { oneOf } from "prop-types";
import { headerMenus, footerMenus, languageMenus } from "../../../modals/page";
import { Button, Tooltip } from "@material-ui/core";
import "./customMenu.scss";
import { useAppContext } from "../../../helpers/AppContext";
import { useTranslation } from "react-i18next";
const CustomMenu = ({ type }) => {
    const { language, setLanguage } = useAppContext();
    const { i18n, t } = useTranslation();
    const getMenus = () => {
        var menu = [];
        switch (type) {
            case "header":
                menu = headerMenus;
                break;
            case "footer":
                menu = footerMenus;
                break;
            case "language":
                menu = languageMenus;
                break;
            default:
                break;
        }
        return menu;
    };
    const onClickLanguageHandler = (event) => {
        setLanguage(event.currentTarget.getAttribute("name"), true);
        i18n.changeLanguage(event.currentTarget.getAttribute("name"));
    };
    return (
        <div className={type}>
            {getMenus().map((menu, index) => {
                console.log(menu);
                return (
                    <React.Fragment>
                        {type === "language" ? (
                            <Button
                                color="primary"
                                variant="contained"
                                size="large"
                                className={menu.slug === language && "active"}
                                name={menu.slug}
                                onClick={onClickLanguageHandler}
                            >
                                <Tooltip title={menu.name}>{menu.icon}</Tooltip>
                            </Button>
                        ) : (
                            <Button
                                color="primary"
                                variant={
                                    type === "footer" ? "text" : "contained"
                                }
                                size="large"
                                className={menu?.slug === language && "active"}
                            >
                                {t(`common:footer.${menu.name}`)}
                            </Button>
                        )}
                    </React.Fragment>
                );
            })}
        </div>
    );
};

/*
 * PropTypes validations
 * @property {oneOf} type - Select the type of menu header or footer or language
 */
CustomMenu.propTypes = {
    mode: oneOf(["header", "footer", "language"]),
};

CustomMenu.defaultProps = {
    type: "header",
};

export default CustomMenu;
