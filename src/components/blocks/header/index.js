import React from "react";
import "./header.scss";
import { AppBar, Container, Toolbar } from "@material-ui/core";
import Logo from "../../shard/logo";
import CustomMenu from "../../shard/customMenu";
const Header = () => {
    return (
        <AppBar position="sticky" color="white">
            <Container maxWidth="lg" className="container mainContainer">
                <Toolbar className="headerWrap">
                    <Logo />
                    <div className="menus">
                        <CustomMenu type="language" />
                    </div>
                </Toolbar>
            </Container>
        </AppBar>
    );
};

export default Header;
