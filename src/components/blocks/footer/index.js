import React from "react";
import "./footer.scss";
import { Container, Typography } from "@material-ui/core";
import Logo from "../../shard/logo";
import CustomMenu from "../../shard/customMenu";
import { useTranslation } from "react-i18next";
const Footer = () => {
    const { t } = useTranslation();
    return (
        <footer className="footerWrap">
            <Container
                maxWidth="lg"
                className="container mainContainer content"
            >
                <Logo mode="plane" />
                <Typography variant="subtitle2" className="footerHeadline">
                    {t("common:footer.making_the_world_a_smaller_place")}
                </Typography>
                <Typography variant="subtitle2" className="copyrightText">
                    {t("common:footer.copyright")}
                </Typography>
                <div className="menus">
                    <CustomMenu type="footer" />
                </div>
            </Container>
        </footer>
    );
};

export default Footer;
