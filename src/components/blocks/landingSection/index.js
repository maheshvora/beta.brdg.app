import React, { useState } from "react";
import {
    Container,
    Typography,
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    TextField,
    DialogActions,
} from "@material-ui/core";
import "./landingSection.scss";
import { useTranslation } from "react-i18next";
import axios from "axios";
import { WAIT_LIST_API } from "../../../helpers/constants";
const LandingSection = () => {
    const { t } = useTranslation();
    const [open, setOpen] = useState(false);
    const [email, setEmail] = useState("");
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);

    const [bridgeAppliedBeta, setBridgeAppliedBeta] = useState(() => {
        const saved = localStorage.getItem("bridge_applied_beta");
        const initialValue = JSON.parse(saved);
        return initialValue || "";
    });
    console.log(bridgeAppliedBeta);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const handleSubscribe = async () => {
        if (validateEmail(email)) {
            setLoading(true);
            setError(false);
            await axios
                .post(WAIT_LIST_API.url, {
                    api_key: WAIT_LIST_API.key,
                    email,
                    referral_link: WAIT_LIST_API.referral_link,
                })
                .then((res) => {
                    console.log(res);
                    setBridgeAppliedBeta(res.data);
                    localStorage.setItem(
                        "bridge_applied_beta",
                        JSON.stringify(res.data)
                    );
                    setLoading(false);
                    setOpen(false);
                })
                .catch((err) => {
                    setLoading(false);
                    setError(t("common:home.something_wrong_error"));
                });
        } else {
            setError(t("common:home.invalid_email_address"));
        }
    };
    const onChangeHandler = (e) => {
        setEmail(e.target.value);
    };

    const validateEmail = (email) => {
        return email.match(
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
    };

    return (
        <Container maxWidth="lg" className="container mainContainer">
            <div className="landingWrap">
                <img src="images/Landing-Image.png" alt="Landing" />
                <Typography variant="h1" className="text">
                    {t("common:home.simple_smart_and_fast_intros")}
                </Typography>
                {bridgeAppliedBeta === "" ? (
                    <Button
                        color="primary"
                        variant="contained"
                        className="btnApply"
                        onClick={handleClickOpen}
                    >
                        {t("common:home.apply_for_beta")}
                    </Button>
                ) : (
                    <Typography variant="h3" className="text">
                        {t("common:home.waiting_list_message", {
                            priority: bridgeAppliedBeta.current_priority,
                        })}
                    </Typography>
                )}
                <Dialog open={open} onClose={handleClose}>
                    <DialogTitle>{t("common:home.apply_for_beta")}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {t("common:home.subscribe_message")}
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label={t("common:home.email_address")}
                            type="email"
                            fullWidth
                            variant="standard"
                            value={email}
                            onChange={onChangeHandler}
                        />
                        <Typography variant="subtitle2" className="error">
                            {error && error}
                        </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            color="primary"
                            variant="outlined"
                            onClick={handleClose}
                        >
                            {t("common:home.cancel")}
                        </Button>
                        <Button
                            color="primary"
                            variant="contained"
                            onClick={handleSubscribe}
                            disabled={loading}
                        >
                            {loading
                                ? t("common:home.loading")
                                : t("common:home.subscribe")}
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        </Container>
    );
};

export default LandingSection;
