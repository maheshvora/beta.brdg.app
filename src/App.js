import { useEffect } from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import "./App.css";
import theme from "./theme";
import { BrowserRouter as Router } from "react-router-dom";
import Routing from "./routing/routing";
import { AppContext, GlobalStore } from "./helpers/AppContext";

function App() {
    const globalStore = GlobalStore();

    useEffect(() => {
        const { setLanguage } = globalStore;

        setLanguage("english", true);
    }, []);
    return (
        <AppContext.Provider value={globalStore}>
            <ThemeProvider theme={theme}>
                <Router>
                    <Routing></Routing>
                </Router>
            </ThemeProvider>
        </AppContext.Provider>
    );
}

export default App;
