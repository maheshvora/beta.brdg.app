import React from "react";
import LandingSection from "../../components/blocks/landingSection";
import DefaultLayout from "../../components/layout/defaultLayout";
import "./home.scss";

const Home = () => {
    return (
        <div className="wrapper">
            <DefaultLayout>
                <LandingSection />
            </DefaultLayout>
        </div>
    );
};

export default Home;
