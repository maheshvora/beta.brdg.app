import i18next from "i18next";
import common_english from "./translations/english/common.json";
import common_french from "./translations/french/common.json";
const formatHandler = (value, format, lng) => {
    if (format === "uppercase") {
        return value.toUpperCase();
    }

    if (format === "lowercase") {
        return value.toLowerCase();
    }

    return value;
};

i18next.init({
    interpolation: {
        escapeValue: false,
        format: formatHandler,
    },
    lng: "english",
    resources: {
        english: {
            common: common_english,
        },
        french: {
            common: common_french,
        },
    },
});

export default i18next;
