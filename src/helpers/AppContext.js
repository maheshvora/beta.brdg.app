import React, { useContext, useState } from "react";

export const GlobalStore = () => {
    // Initialize Global States
    const [language, setLanguage] = useState("english");

    const setContext = (value, setState, reset) => {
        if (reset) {
            return setState(value);
        }
        return setState((prevState) => {
            return {
                ...prevState,
                ...value,
            };
        });
    };

    // context getters
    const getters = {
        language,
    };

    // context setters
    const setters = {
        setLanguage: (value, reset) => {
            return setContext(value, setLanguage, reset);
        },
    };

    return {
        ...getters,
        ...setters,
    };
};

export const AppContext = React.createContext({});
export const useAppContext = () => useContext(AppContext);
