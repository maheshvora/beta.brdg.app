import React from "react";
import { Route, Routes } from "react-router-dom";
import { links } from "../modals/page";
export default function Routing() {
    return (
        <Routes>
            {links.map((page, index) => {
                return (
                    <Route
                        key={index}
                        exact
                        {...page}
                        state={{ pageName: page.menu }}
                    />
                );
            })}
        </Routes>
    );
}
